FROM rteja91/aws-cli:latest
RUN curl -sL https://rpm.nodesource.com/setup_14.x |  bash -
RUN yum install -y nodejs gcc-c++ make python2
RUN node --version && npm --version

RUN mkdir /release && \
    groupadd --gid 1000 ec2-user && useradd --create-home --uid 1000 --gid 1000 --comment "Builder user" ec2-user

COPY entrypoint /release

RUN chmod +x /release/entrypoint && \
    dos2unix /release/entrypoint

USER ec2-user

ENTRYPOINT [ "/release/entrypoint" ]